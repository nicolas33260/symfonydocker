Symfony Docker
==============

Boiler for **Symfony 4.4 LTS** and **Docker**.

![](https://camo.githubusercontent.com/7314d8f36d2bc7052b0f451e8816749270620e87/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f5048505374616e2d656e61626c65642d627269676874677265656e2e7376673f7374796c653d666c6174)


## Stack
- PHP 7.4-FPM
- NGINX 1.17-alpine
- MySQL 8
- Minio
- REDIS 5-alpine
- Adminer

## Installation
```bash
git clone git@bitbucket.org:nicolas33260/symfonydocker.git --project-name--

cp .env.dist .env

#Edition des variables d'env
nano .env

#Suppression de l'origin pour ratacher le projet à un nouveau git
git remote rm origin
```

## Development
```bash
docker-compose up -d
docker exec -ti symfony_dev bash

#In the container
composer install
```

## Production

    sh build.sh version #exemple sh build.sh 1.0.0

## Commandes

Dans le container **symfony_dev**

```bash
stan # Lance l'analyse avec PHPStan
ecs # Fix le dossier src avec PHP-CS-FIXER
run-test # Lance les tests du projet
```

Inspiration https://github.com/TrafeX/docker-php-nginx
