<?php

namespace App\Tests\Base;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\AbstractBrowser;
use Symfony\Component\Console\Application;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class AbstractTest
 *
 * @package App\Tests\Base
 */
class AbstractTest extends WebTestCase
{
    /**
     * @var ContainerInterface
     */
    protected static $container;

    /**
     * @var Application
     */
    protected static $application;

    /**
     * @var EntityManagerInterface
     */
    protected static $manager;

    /**
     * @var AbstractBrowser
     */
    protected $client;

    public static function setUpBeforeClass(): void
    {
        $kernel = new \App\Kernel('test', true);
        $kernel->boot();

        self::$container = $kernel->getContainer();

        self::$manager = self::$container->get('doctrine.orm.entity_manager');
    }

    protected function initialize()
    {
        $this->client = static::createClient();
    }

    /**
     * @param string      $uri
     * @param array       $params
     * @param string|null $content
     * @param array       $files
     * @param array       $headers
     *
     * @return Crawler
     */
    protected function get(string $uri, array $params = [], string $content = null, array $files = [], array $headers = []): Crawler
    {
        return $this->client->request('GET', $uri, $params, $files, [], $content);
    }

    /**
     * @param string      $uri
     * @param array       $params
     * @param string|null $content
     * @param array       $files
     * @param array       $headers
     *
     * @return Crawler
     */
    protected function post(string $uri, array $params = [], string $content = null, array $files = [], array $headers = []): Crawler
    {
        return $this->client->request('POST', $uri, $params, $files, [], $content);
    }

    /**
     * @param string      $uri
     * @param array       $params
     * @param string|null $content
     * @param array       $files
     * @param array       $headers
     *
     * @return Crawler
     */
    protected function put(string $uri, array $params = [], string $content = null, array $files = [], array $headers = []): Crawler
    {
        return $this->client->request('PUT', $uri, $params, $files, [], $content);
    }

    /**
     * @param string      $uri
     * @param array       $params
     * @param string|null $content
     * @param array       $files
     * @param array       $headers
     *
     * @return Crawler
     */
    protected function patch(string $uri, array $params = [], string $content = null, array $files = [], array $headers = []): Crawler
    {
        return $this->client->request('PATCH', $uri, $params, $files, [], $content);
    }

    /**
     * @param bool $json
     *
     * @return mixed|object
     */
    protected function getResponseContent($json = false)
    {
        if ($json) {
            return json_decode($this->client->getResponse()->getContent(), true);
        }

        return $this->client->getResponse()->getContent();
    }

    /**
     * @return int
     */
    protected function getStatusCode()
    {
        return $this->client->getResponse()->getStatusCode();
    }

    /**
     *
     */
    protected function tearDown(): void
    {
        self::$manager->clear();
    }
}
