#!/bin/bash

php bin/console d:d:c --if-not-exists
php bin/console d:s:u -f

./vendor/bin/simple-phpunit --process-isolation \
    --stop-on-error \
    --stop-on-failure \
    --verbose \
    --coverage-text \
    --colors=never \
    --coverage-clover tests/coverage-clover.xml \
    --log-junit tests/unitests-results.xml
