<?php

declare(strict_types=1);

use PhpCsFixer\Fixer\ArrayNotation\ArraySyntaxFixer;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symplify\EasyCodingStandard\ValueObject\Option;
use Symplify\EasyCodingStandard\ValueObject\Set\SetList;

return static function (ContainerConfigurator $containerConfigurator): void {
    // A. standalone rule
    $services = $containerConfigurator->services();
    $services->set(ArraySyntaxFixer::class)
        ->call('configure', [[
            'syntax' => 'short',
        ]]);

    // B. full sets
    $parameters = $containerConfigurator->parameters();
    $parameters->set(Option::SETS, [
        SetList::CLEAN_CODE,
        SetList::PSR_12,
        SetList::SYMFONY,
        SetList::PHP_71,
        SetList::COMMENTS,
        SetList::DOCBLOCK,
        SetList::CONTROL_STRUCTURES,
        SetList::PHP_CS_FIXER,
        SetList::DEAD_CODE,
        SetList::PSR_1,
        SetList::PHP_73_MIGRATION,
        SetList::SYMFONY_RISKY
    ]);
};