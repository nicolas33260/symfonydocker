<?php

declare(strict_types=1);

namespace App\Controller;

use League\Flysystem\FilesystemInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Class MinIOController.
 */
class MinIOController extends AbstractController
{
    public function indexAction(FilesystemInterface $usersStorage): void
    {
        $usersStorage->createDir('test');
    }
}
