<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class HealthCheckController.
 */
class HealthCheckController extends AbstractController
{
    /**
     * @Route(path="/healthcheck", name="healthcheck")
     */
    public function healthCheckAction(): JsonResponse
    {
        return $this->json([
            'api' => 'available',
        ]);
    }
}
