#!/bin/bash

apt-get update --fix-missing
apt-get install git zip curl libzip-dev -yqq

docker-php-ext-install pdo_mysql zip

curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
cd symfony && composer install
chmod a+x tests/run.sh && tests/run.sh
chmod a+x tools/quality.sh && tools/quality.sh
