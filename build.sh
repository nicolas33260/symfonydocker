#!/bin/bash

PROD_PATH=./docker/prod/symfony

echo "COPY symfony folder to $PROD_PATH"
cp -Rf ./symfony $PROD_PATH

echo "REMOVE Git, Cache, Vendor and Idea folder"
rm -Rf $PROD_PATH/.git
rm -Rf $PROD_PATH/.gitignore
rm -Rf $PROD_PATH/phpstan.neon
rm -Rf $PROD_PATH/phpunit.xml.dist
rm -Rf $PROD_PATH/config/packages/dev
rm -Rf $PROD_PATH/config/packages/test
rm -Rf $PROD_PATH/config/routes/dev
rm -Rf $PROD_PATH/var/cache/*
rm -Rf $PROD_PATH/var/log/*
rm -Rf $PROD_PATH/.idea
rm -Rf $PROD_PATH/vendor
rm -Rf $PROD_PATH/test
rm -Rf $PROD_PATH/bin/phpunit

docker-compose -f docker-compose-prod.yaml build

echo "REMOVE Temp $PROD_PATH"
rm -Rf $PROD_PATH

docker login "${CONTAINER_REGISTRY}" --username "${CONTAINER_REGISTRY_USER}" --password "${CONTAINER_REGISTRY_PASSWORD}"
docker tag "${DOCKER_PRODUCTION_CONTAINER_NAME}" "${CONTAINER_REGISTRY_URL}"/"${CONTAINER_REGISTRY_NAME}":"$1"
docker push "${CONTAINER_REGISTRY_URL}"/"${CONTAINER_REGISTRY_NAME}":"$1"
